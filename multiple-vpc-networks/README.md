# Multiple VPC Networks

## Overview

In this lab, you create several VPC networks and VM instances and test connectivity across networks. Specifically, you create two custom mode networks (managementnet and privatenet) with firewall rules and VM instances as shown in this network diagram:

![Schema of VPC Networks in the lab](../resources/vpc-schema.png)



## Setup and Requirements

```sh
gcloud auth list
gcloud config list project
```

## Create custom mode VPC networks with firewall rules


* managementnet and managementsubnet-us

```sh
gcloud compute --project=qwiklabs-gcp-04-272cf33d2201 networks create managementnet --subnet-mode=custom

gcloud compute --project=qwiklabs-gcp-04-272cf33d2201 networks subnets create managementsubnet-us --network=managementnet --region=us-central1 --range=10.130.0.0/20
```

* privatenet and privatesubnet-us and privatesubnet-eu

```sh
gcloud compute networks create privatenet --subnet-mode=custom
gcloud compute networks subnets create privatesubnet-us --network=privatenet --region=us-central1 --range=172.16.0.0/24
gcloud compute networks subnets create privatesubnet-eu --network=privatenet --region=europe-west1 --range=172.20.0.0/20
```

```sh
gcloud compute networks list
gcloud compute networks subnets list --sort-by=NETWORK
```

* Create firewall rules

```sh
gcloud compute firewall-rules create managementnet-allow-icmp-ssh-rdp --direction=INGRESS --priority=1000 --network=managementnet --action=ALLOW --rules=tcp:22,tcp:3389,icmp --source-ranges=0.0.0.0/0 --project=qwiklabs-gcp-04-272cf33d2201

gcloud compute firewall-rules create privatenet-allow-icmp-ssh-rdp --direction=INGRESS --priority=1000 --network=privatenet --action=ALLOW --rules=icmp,tcp:22,tcp:3389 --source-ranges=0.0.0.0/0
```

```sh
gcloud compute firewall-rules list --sort-by=NETWORK
```



## Create VM instances

Create two VM instances:

* managementnet-us-vm in managementsubnet-us

```sh
gcloud beta compute instances create managementnet-us-vm --zone=us-central1-c --machine-type=f1-micro --subnet=managementsubnet-us --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=392848588620-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --image=debian-10-buster-v20200413 --image-project=debian-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=managementnet-us-vm --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any --project=qwiklabs-gcp-04-272cf33d2201
```

* privatenet-us-vm in privatesubnet-us

```sh
gcloud compute instances create privatenet-us-vm --zone=us-central1-c --machine-type=n1-standard-1 --subnet=privatesubnet-us
```

```sh
gcloud compute instances list --sort-by=ZONE
ping -c 3 34.76.4.149
```

## Explore the connectivity between VM instances

## Create a VM instance with multiple network interfaces

```sh
gcloud beta compute instances create vm-appliance --zone=us-central1-c --machine-type=n1-standard-4 --subnet=privatesubnet-us --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=392848588620-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --image=debian-10-buster-v20200413 --image-project=debian-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=vm-appliance --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any
```

In the vm-appliance:

```sh
sudo ifconfig
ip route
```

## Congratulations!
