# Managing Deployments using Kubernetes Engine

The lab is [here][1].

## Overview

## Introduction to deployments

## Setup

```sh
gcloud auth list
gcloud config list project
gcloud config set compute/zone us-central1-a
git clone https://github.com/googlecodelabs/orchestrate-with-kubernetes.git
cd orchestrate-with-kubernetes/kubernetes
gcloud container clusters create bootcamp --num-nodes 5 --scopes "https://www.googleapis.com/auth/projecthosting,storage-rw"
```

## Learn about the deployment object

```sh
kubectl explain deployment # tell us about the Deployment object
kubectl explain deployment --recursive # se all fields
kubectl explain deployment.metadata.name # understand what the individual fields do
```

## Create a deployment

```sh
vi deployments/auth.yaml # Edit the file
````

```yaml
...
containers:
- name: auth
  image: kelseyhightower/auth:1.0.0
...
```

```sh
kubectl create -f deployments/auth.yaml # Create the deployment
kubectl get deployments # verify the deployment has been created
kubectl get replicasets # verify the replicaset
kubectl get pods # verify the pods
kubectl create -f services/auth.yaml # create the service
kubectl create -f deployments/hello.yaml
kubectl create -f services/hello.yaml # create and expose the hello Deployment
```

* Create the frontend deployment

```sh
kubectl create secret generic tls-certs --from-file tls/
kubectl create configmap nginx-frontend-conf --from-file=nginx/frontend.conf
kubectl create -f deployments/frontend.yaml
kubectl create -f services/frontend.yaml
```

**Note**: You created a ConfigMap for the frontend.
Interact with the frontend by grabbing its external IP and then curling to it.

```sh
kubectl get services frontend
curl -ks https://34.66.156.241
curl -ks https://`kubectl get svc frontend -o=jsonpath="{.status.loadBalancer.ingress[0].ip}"`
```

* Scale a deployment

```sh
kubectl explain deployment.spec.replicas
kubectl scale deployment hello --replicas=5
kubectl get pods | grep hello- | wc -l # verify there are now 5 hello Pods
kubectl scale deployment hello --replicas=3 # scale back the application
```

## Rolling update

Deployments support updating images to a new version through a rolling update mechanism. When a Deployment is updated with a new version, it creates a new ReplicaSet and slowly increases the number of replicas in the new ReplicaSet as it decreases the replicas in the old ReplicaSet.

![Replicaset](../resources/replicaset.png)

```sh
kubectl edit deployment hello # update the deployment
```

```yaml
...
containers:
- name: hello
  image: kelseyhightower/hello:2.0.0
...
```

```sh
kubectl get replicaset
kubectl rollout history deployment/hello
```

* Pause a rolling update

```sh
kubectl rollout pause deployment/hello
kubectl rollout status deployment/hello # verify the status of the rollout
kubectl get pods -o jsonpath --template='{range .items[*]}{.metadata.name}{"\t"}{"\t"}{.spec.containers[0].image}{"\n"}{end}'
```

* Resume a rolling update

```sh
kubectl rollout resume deployment/hello
kubectl rollout status deployment/hello
```

* Rollback an update

```sh
kubectl rollout undo deployment/hello # rollback to the previous version
kubectl rollout history deployment/hello # verify the version
kubectl get pods -o jsonpath --template='{range .items[*]}{.metadata.name}{"\t"}{"\t"}{.spec.containers[0].image}{"\n"}{end}' # verify all the pods have rolled back to their previous versions
```

## Canary deployments

### Create a canary deployment
A canary deployment consists of a separate deployment with your new version and a service that targets both your normal, stable deployment as well as your canary deployment.

![Canary Deployment](../resources/canary.png)

```sh
cat deployments/hello-canary.yaml
kubectl create -f deployments/hello-canary.yaml # create the canary deployment
kubectl get deployments # you should have two deployments (hello and hello-canary)
```

On the `hello` service, the selector uses the `app:hello` selector which will match **pods** in both the prod deployment and canary deployment. However, because the canary deployment has a fewer number of pods, it will be visible to fewer users

### Verify the canary deployment

```sh
curl -ks https://`kubectl get svc frontend -o=jsonpath="{.status.loadBalancer.ingress[0].ip}"`/version
```

Run this several times and you should see that some of the requests are served by hello 1.0.0 and a small subset (1/4 = 25%) are served by 2.0.0.

### Canary deployments in production - session affinity

## Blue-green deployments

![Blue Green Deployment](../resources/blue-green.png)

### The service

Update the service.

```sh
kubectl apply -f services/hello-blue.yaml
```

### Updating using Blue-Green Deployment

```sh
kubectl create -f deployments/hello-green.yaml # create the green deployment
curl -ks https://`kubectl get svc frontend -o=jsonpath="{.status.loadBalancer.ingress[0].ip}"`/version # verify that the current version of 1.0.0 is still being used
kubectl apply -f services/hello-green.yaml # update the service to point to the new version
curl -ks https://`kubectl get svc frontend -o=jsonpath="{.status.loadBalancer.ingress[0].ip}"`/version # the new version is always being used
```

### Blue-Green Rollback

```sh
kubectl apply -f services/hello-blue.yaml
curl -ks https://`kubectl get svc frontend -o=jsonpath="{.status.loadBalancer.ingress[0].ip}"`/version
```

## Congratulations!

[1]: https://google.qwiklabs.com/focuses/639?parent=catalog