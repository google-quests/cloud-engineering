# Introduction to SQL for BigQuery and Cloud SQL

## The basics of SQL

```sql
SELECT USER FROM example_table
SELECT USER, SHIPPED FROM example_table
SELECT USER FROM example_table WHERE SHIPPED='YES'
```

## Exploring the BigQuery Console

Include the Dataset

* GCP Project → bigquery-public-data
* Dataset → london_bicycles
* Table → cycle_hire

```sql
SELECT end_station_name FROM `bigquery-public-data.london_bicycles.cycle_hire`;
SELECT * FROM `bigquery-public-data.london_bicycles.cycle_hire` WHERE duration>=1200;
```

## Other keywords

```sql
SELECT
  start_station_name
FROM
  `bigquery-public-data.london_bicycles.cycle_hire`
GROUP BY
  start_station_name;
```

## More SQL Keywords

```sql
SELECT start_station_name, COUNT(*) FROM `bigquery-public-data.london_bicycles.cycle_hire` GROUP BY start_station_name;
SELECT start_station_name, COUNT(*) AS num_starts FROM `bigquery-public-data.london_bicycles.cycle_hire` GROUP BY start_station_name;
```

```sql
SELECT start_station_name, COUNT(*) AS num FROM `bigquery-public-data.london_bicycles.cycle_hire` GROUP BY start_station_name ORDER BY start_station_name;
SELECT start_station_name, COUNT(*) AS num FROM `bigquery-public-data.london_bicycles.cycle_hire` GROUP BY start_station_name ORDER BY num;
SELECT start_station_name, COUNT(*) AS num FROM `bigquery-public-data.london_bicycles.cycle_hire` GROUP BY start_station_name ORDER BY num DESC;
```

## Working with Cloud SQL

```sql
SELECT end_station_name, COUNT(*) AS num FROM `bigquery-public-data.london_bicycles.cycle_hire` GROUP BY end_station_name ORDER BY num DESC;
```

Create a MySQL instance named `apecr-mysql-instance`.

## New Queries in Cloud SQL

```sqh
gcloud auth list
gcloud config list project
gcloud sql connect  apecr-mysql-instance --user=root
```

```sql
CREATE DATABASE bike;
USE bike;
CREATE TABLE london1 (start_station_name VARCHAR(255), num INT);
USE bike;
CREATE TABLE london2 (end_station_name VARCHAR(255), num INT);
SELECT * FROM london1;
SELECT * FROM london2;
```

```sql
DELETE FROM london1 WHERE num=0;
DELETE FROM london2 WHERE num=0;
```

```sql
INSERT INTO london1 (start_station_name, num) VALUES ("test destination", 1);
```

```sql
SELECT start_station_name AS top_stations, num FROM london1 WHERE num>100000
UNION
SELECT end_station_name, num FROM london2 WHERE num>100000
ORDER BY top_stations DESC;
```